#### Dynamic Inventory for EC2 Servers

Lets say we have ec2 servers which keep on getting added and deleted all the time, lets take auto-scaling in play to meet business demands, so therefore having statis IP addresses in the host is not practical. So in this project we will create 3 ec2 instances with Terraform and connect to these servers with Ansible without hardcoding the IP addresses.

#### Lets get started

Using terraform files from previous projects going to create three instances

![Image 1](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image1.png)

And then third one will be with a t2.small

![Image 2](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image2.png)

Can start of with initialising

```
terraform init
```

![Image 3](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image3.png)

Can then do

```
terraform apply –auto-approve
```

And can see they are all running

![Image 4](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image4.png)

Since we do not want to get the IP’s of the ec2 instances and add them to the host, to overcome this we need a functionality that would connect to the AWS account and get the server information. There are two ways to achieve this, Inventory Plugins and Inventory Scripts.

Looking at the ansible docs can see the below

![Image 5](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image5.png)

The take home points is that it is more advantageous use plugins over scripts, reason being is that you can use built ansible features like state management and are written in YAML where as the scripts are written in Python.
So in this project we will go with the plugin option. To proceed we need a plugin specific for the infrastructure provider in this case it will be AWS.
So now lets find a aws_ec2 plugin inventory, Can use the below documentation

https://docs.ansible.com/ansible/latest/collections/amazon/aws/aws_ec2_inventory.html

![Image 6](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image6.png)

Note to use the plugin we need boto3 and botocore, having utilised python with AWS  this has already been configured.

Now in the ansible.cfg we need to enable the plugin just like the below

![Image 7](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image7.png)

Now lets write the plugin configuration with the plugin name included

![Image 8](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image8.png)

Can then configure the inventory like the below with the region where our ec2 instances reside

![Image 9](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image9.png)

Now we can use an inventory command which will display the resources in that aws region

Can run the below

```
ansible-inventory -i inventory_aws_ec2.yaml --list
```

![Image 10](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image10.png)

Going down below we can see the configured hosts for which ansible will use to connect to the instances

![Image 11](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image11.png)

One thing to note on the above is that they are private DNS names, 

However we need it to return public IP’s or public DNS hostnames so that we can connect to the ec2 instances outside of the VPC instead if inside of the VPC.

So if the inventory does not have public inventory assigned it will use the private one instead

Again using the below link

https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc

Can use the below enable dns hostname

![Image 12](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image12.png)

Can then configure the below on the VPC level

![Image 13](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image13.png)

And then apply the terraform script

```
terraform apply –auto-approve
```

Can see the resources getting created

![Image 14](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image14.png)

Can then run the below command

```
ansible-inventory -i inventory_aws_ec2.yaml –graph
```

![Image 15](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image15.png)

Now lets configure ansible to use the inventory in the playbooks

So instead of our hosts file, we want the playbook to use the inventory file

This will be the group that will be referenced

![Image 16](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image16.png)

Can then also set the hosts

![Image 17](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image17.png)

Can then set the below in the ansible configuration file globally

![Image 18](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image18.png)

Can then use the below command to check the connection

```
ansible-playbook – i inventory_aws_ec2.yaml deploy-docker-ubuntu-new-user.yaml
```

![Image 19](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image19.png)

![Image 20](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image20.png)

Can then see the  playbook completed the script simultaneously on all servers

![Image 21](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image21.png)

Adding the fourth server

![Image 22](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image22.png)

And hit terraform apply

Can see the newly created fourth server

![Image 24](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image24.png)

Then running the playbook command again can see it connected successfully

![Image 25](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image25.png)

Can then also see it deployed the playbook to the new server

![Image 26](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image26.png)

![Image 27](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image27.png)

If we were to only use the inventory_aws_ec2.yaml as the only host file, can do it like the below

![Image 28](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image28.png)

Can then just apply the below command

```
ansible-playbook – i inventory_aws_ec2.yaml deploy-docker-ubuntu-new-user.yaml
```

![Image 29](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image29.png)

And can see it works

![Image 30](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image30.png)

Now lets say we only want to target specific servers

Let’s destroy the current configuration and create two dev servers and two prod servers

![Image 31](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image31.png)

```
terraform apply –auto-approve
```

And can see it is now running

![Image 32](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image32.png)

Now going back to the documentation

https://docs.ansible.com/ansible/latest/collections/amazon/aws/aws_ec2_inventory.html

Can see one of the features is filters

And then navigating to the AWS site can see the filters

https://docs.aws.amazon.com/cli/latest/reference/ec2/describe-instances.html

![Image 33](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image33.png)

Can the use the below

![Image 34](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image34.png)

Can then do the below with ‘tag:Name’

![Image 35](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image35.png)

Can then run the inventory graph command to filter the one’s with dev key

Can then test it using the below

```
ansible-inventory -I inventory_aws_ec2.yaml –graph
```

![Image 36](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image36.png)

Can also add in a filter for running instances just like the below

![Image 37](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image37.png)

Can also differentiate between the prod and dev servers using a filter called ‘keyed_groups’ as seen below in the documentation

![Image 38](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image38.png)

Can configure it like the below using tags

![Image 39](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image39.png)

Can then run the below

```
ansible-inventory -I inventory_aws_ec2.yaml –graph
```

And below are the results

![Image 40](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image40.png)

Also adding a tag a prefix

![Image 41](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image41.png)

Now let’s execute for the dev servers only

![Image 42](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image42.png)

Can then run the below command and can see it is only configuring the dev servers

```
ansible-playbook deploy-docker-ubuntu-new-user.yaml
```

![Image 43](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image43.png)

And can see it has completed

![Image 44](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image44.png)

Can also do it for instance type as well using the key and prefix

![Image 45](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image45.png)

Can see the below grouping which can be referenced in the hosts

```
ansible-inventory -I inventory_aws_ec2.yaml –graph
```

![Image 47](https://gitlab.com/FM1995/ansible-terraform-dynamic-inventory-with-aws-ec2/-/raw/main/Images/Image47.png)










